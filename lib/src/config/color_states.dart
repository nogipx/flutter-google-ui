import 'dart:ui';

extension ColorState on Color {
  get hover => this.withOpacity(.04);
  get focus => this.withOpacity(.12);
  get selected => this.withOpacity(.08);
  get activated => this.withOpacity(.12);
  get pressed => this.withOpacity(.12);
  get dragged => this.withOpacity(.08);

  get hoverDark => this.withOpacity(.08);
  get focusDark => this.withOpacity(.24);
  get selectedDark => this.withOpacity(.16);
  get activatedDark => this.withOpacity(.24);
  get pressedDark => this.withOpacity(.24);
  get draggedDark => this.withOpacity(.16);
}